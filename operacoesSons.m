function operacoesSons(opcao)
    if (opcao==1)
        infoMutua=calculaInfoMutua([2 6 4 10 5 9 5 8 0 8],[6 8 9 7 2 4 9 9 4 9 1 4 8 0 1 2 2 6 3 2 0 7 4 9 5 4 8 5 2 7 8 0 7 4 8 5 7 4 3 2 2 7 3 5 2 7 4 9 9 6],0:10,1);
        fprintf('Info Mutua=');
        for i=1:length(infoMutua)
            fprintf(' %.4f',infoMutua(i));
            if mod(i,10)==0
                fprintf('\n');
            end;
        end;
        fprintf('\n');
    end
    
    if (opcao==2)
        [fonte,f,quant] = wavread('dados/anexos/guitarSolo.wav');
        query = fonte(:,1);
        d = 2/(2^quant);
        alfabeto = -1:d:1-d;
        
        [fonte,f,quant] = wavread('dados/sounds/target01 - repeat.wav');
        target1 = fonte(:,1);

        [fonte,f,quant] = wavread('dados/sounds/target02 - repeatNoise.wav');
        target2 = fonte(:,1);
        fprintf('***Calculando Informacao Mutua para repeat.wav***\n');
        infoMutua1=calculaInfoMutua(query,target1,alfabeto,round(length(query)/4));
        %disp(infoMutua1)
        p1=plot(1:length(infoMutua1),infoMutua1);
        set(p1,'Color','blue','LineWidth',3)
        xlabel('Tempo')
        ylabel('Informa��o M�tua')
        title('Informa��o M�tua de guitarSolo.wav e repeat.wav')        
        
        fprintf('\nPrima uma tecla para continuar\n\n');
        pause 
        fprintf('***Calculando Informacao Mutua para repeatNoise.wav***\n');
        infoMutua2=calculaInfoMutua(query,target2,alfabeto,round(length(query)/4));
        %disp(infoMutua2)

        p2=plot(1:length(infoMutua2),infoMutua2);
        set(p2,'Color','blue','LineWidth',3)
        xlabel('Tempo')
        ylabel('Informa��o M�tua')
        title('Informa��o M�tua de guitarSolo.wav e repeatNoise.wav')
    end
    
    if (opcao==3)
        maxInfoMutua=zeros(7,2);
        [fonte,f,quant] = wavread('dados/anexos/guitarSolo.wav');
        query = fonte(:,1);
        d = 2/(2^quant);
        alfabeto = -1:d:1-d;
        step=length(query)/4;
        
        for i=1:7
            ficheiro=sprintf('dados/sounds/Song0%d.wav',i);
            [fonte,f,quant] = wavread(ficheiro);
            target = fonte(:,1);
            infoMutua=calculaInfoMutua(query,target,alfabeto,step);
            maxInfoMutua(i,2)=i;
            maxInfoMutua(i,1)=max(infoMutua);
            %fprintf('Iteracao %d\n', i);
            fprintf('Informacao Mutua Maxima para Song0%d: %0.4f bits\n', i,maxInfoMutua(i,1));
            %disp(infoMutua);
            %inforMutua=infoMutua(infoMutua~=0);
            %disp(inforMutua);
        end;
        sortMaxInfoMutua = sortrows(maxInfoMutua,-1);
        fprintf('\nInformacao Mutua Maxima por Ordem Decrescente:\n');
        for i=1:7
            fprintf('%s: %0.4f  bits\n', sprintf('Song0%d',sortMaxInfoMutua(i,2)), sortMaxInfoMutua(i,1));
        end
    end
end