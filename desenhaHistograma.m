function desenhaHistograma(dicionario,ocorrencias,ficheiro)
    bar(dicionario,ocorrencias);
    texto = ['Histograma de ' ficheiro];
    title(texto);
    xlabel('Simbolos');
    ylabel('Ocorrencias');
end