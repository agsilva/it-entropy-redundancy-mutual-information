function operacoesAgrupadas(ficheiro)
    texto = ['A ler o ficheiro ' ficheiro ' com um tratamento agrupado.'];
    disp('-----------------------');
    disp(texto);
    file=['dados/anexos/' ficheiro];
    [fonte,alf]=lerDados(file);
    ocorrencias=calculaOcorrenciasAgrupadas(fonte,alf);
    probabilidade = ocorrencias ./ sum(ocorrencias);
    lim=minimo(probabilidade);
    disp('Numero medio de bits por agrupamento de simbolos:');
    disp(lim/2)
end