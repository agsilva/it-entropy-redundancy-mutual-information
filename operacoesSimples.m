function operacoesSimples(ficheiro)
    texto = ['A ler o ficheiro ' ficheiro ' com um tratamento simples.'];
    disp('-----------------------');
    disp(texto);
    file=['dados/anexos/' ficheiro];
    [fonte,alf]=lerDados(file);
    ocorrencias=calculaOcorrencias(fonte,alf);
    probabilidade = ocorrencias ./ sum(ocorrencias);
    lim=minimo(probabilidade);
    disp('Numero medio de bits por simbolo:');
    disp(lim);
    lim_huff=minimoHufflen(ocorrencias,probabilidade);
    disp('Numero medio de bits por simbolo usando Hufflen:');
    disp(lim_huff);
    desenhaHistograma(alf,ocorrencias,ficheiro);
end