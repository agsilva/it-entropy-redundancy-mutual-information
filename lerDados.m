function [fonte,alf] = lerDados(ficheiro)
    ext = ficheiro(end-2:end);
    if(isequal(ext,'wav') == 1)
        [fonte,f,quant] = wavread(ficheiro);
        fonte = fonte(:)';
        d = 2/(2^quant);
        alf = -1:d:1-d;
    else
        if(isequal(ext,'bmp') == 1)
            fonte = imread(ficheiro);
            fonte = fonte(:)';
            info = imfinfo(ficheiro);
            alf = 0:2^info.BitDepth-1;
            else
            if(isequal(ext,'txt') == 1)
                file = fopen(ficheiro);
                [num_tmp,count] = fscanf(file, '%s');
                fonte=zeros(1,count);
                alf=double(['A':'Z','a':'z','.',',']);
                for i=1:count
                    fonte(i)=double(num_tmp(i));
                end
                fclose(file);
            end
        end
    end
end