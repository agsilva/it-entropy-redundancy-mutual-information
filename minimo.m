function [lim] = minimo(probabilidade)
    prob2 = probabilidade(probabilidade~=0);
	lim = - sum(prob2 .* log2(prob2));
end