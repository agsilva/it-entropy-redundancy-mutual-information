function [Ocorr]=calculaOcorrencias(dados,dicionario)
Ocorr=zeros(1,length(dicionario));
for i=1:length(dicionario)
    Ocorr(i)=length(find(dados==dicionario(i)));
end
