function [Ocorr]=calculaOcorrenciasAgrupadas(dados,dicionario)

Ocorr=zeros(1,length(dicionario)^2);
dicionarioAgrupado=zeros([2 length(dicionario)^2]);
pos=1;
if(mod(length(dados),2)~=0)
    dados=[dados 0];
end
for i=1:length(dicionario)
    for j=1:length(dicionario)
        dicionarioAgrupado(1,pos)=dicionario(i);
        dicionarioAgrupado(2,pos)=dicionario(j);
        pos=pos+1;
    end

end
for i=1:length(dicionarioAgrupado)
    for j=1:2:(length(dados)-1)
        if(dados(j)==dicionarioAgrupado(1,i) && dados(j+1)==dicionarioAgrupado(2,i))
            Ocorr(i)=Ocorr(i)+1;
        end
    end
end
