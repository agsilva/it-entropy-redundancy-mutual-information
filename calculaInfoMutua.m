function [infoMutua]=calculaInfoMutua(query,target,alfabeto,step)
   parcial=0;
   total=0;
   infoMutua=zeros(1,ceil((length(target)-length(query))/step));
   probY= calculaOcorrencias(query,alfabeto);
   probY= probY ./ length(query);
   for i=1:step:(length(target)-length(query)+1)
       pXY=probCondicionada(target(i:(i+length(query)-1)),query,alfabeto);
       probX=calculaOcorrencias(target(i:(i+length(query)-1)),alfabeto);
       probX=probX ./ length(query);
        
        for y=1:length(alfabeto)
            for x=1:length(alfabeto)
               
                if (pXY(x,y)~=0 && probX(x)~=0 && probY(y)~=0)
                    parcial= parcial + pXY(x,y)* log2( pXY(x,y)/(probX(x)*probY(y)));
                end;
            end;
            total=total+parcial;
            parcial=0;
        end;
        infoMutua(i)=total;
        total=0;
   end;
end
