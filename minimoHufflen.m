function [lim_huff] = minimoHufflen(ocorrencias,probabilidade)
    huff = hufflen(ocorrencias);
    lim_huff = sum(huff .* probabilidade);
end
