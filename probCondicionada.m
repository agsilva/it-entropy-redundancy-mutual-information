function [matriz]=probCondicionada(target, query, alfabeto)
    matriz=zeros([length(alfabeto) length(alfabeto)]);
    
    for j=1:length(query)
        posX= find(alfabeto==target(j));
        posY= find(alfabeto==query(j));
        matriz(posX,posY)=matriz(posX,posY)+1;
    end
    
    total=sum(matriz);
    total=sum(total);
    matriz=matriz ./ total;
end
